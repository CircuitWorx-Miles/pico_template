/* pico_template/src/main.c
 * written in 2021 by Miles Courtie for CircuitWorx
 */

#include "pico/stdlib.h"

#include "LedControl.h"

int main() {
    uint8_t i;

    LedControl_init();

    while (true) {
        for (i = 1; i <= 5; i++) {
            LedControl_blink(i);
            sleep_ms(2000);
        }
        sleep_ms(5000);
    }
}