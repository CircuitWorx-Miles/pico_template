/* pico_template/src/LedControl.c
 * written in 2021 by Miles Courtie for CircuitWorx
 */

#include "LedControl.h"
#include "LedBlink.pio.h"

#include "pico/stdlib.h"
#include "hardware/irq.h"
#include "hardware/pio.h"



/*==================== constants ====================*/

static const uint8_t LED_PIN = 25;
static const uint8_t PIO_IRQ_NUM = 0;
static const uint8_t SYS_IRQ_NUM = PIO0_IRQ_0;
static const PIO PIO_INST = pio0;


/*==================== globals ====================*/

static bool g_busy;
static uint g_stateMachine;



/*==================== static functions ====================*/

/* ISR for when PIO is done blinking LED */
static void onBlinkFinished() {
    g_busy = false;
    pio_interrupt_clear(PIO_INST, PIO_IRQ_NUM);
}



/*==================== interface functions ====================*/

void LedControl_init() {
    uint8_t offset;

    g_busy = false;

    g_stateMachine = pio_claim_unused_sm(PIO_INST, true);
    offset = pio_add_program(PIO_INST, &LedBlink_program);

    irq_set_enabled(SYS_IRQ_NUM, true);
    irq_set_exclusive_handler(SYS_IRQ_NUM, &onBlinkFinished);

    LedBlink_program_init(PIO_INST, g_stateMachine, offset, LED_PIN);    
}

void LedControl_blink(uint32_t count) {
    
    // wait until not busy
    while (g_busy) { ; }

    // wait for tx fifo to not be full
    while (pio_sm_is_tx_fifo_full(PIO_INST, g_stateMachine)) { ; }

    // write value to tx fifo
    pio_sm_put(PIO_INST, g_stateMachine, count);

    // set busy flag
    g_busy = true;
}