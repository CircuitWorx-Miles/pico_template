# Pi Pico Template Project

This project aims to show what's required to program the Raspberry Pi Pico using its C SDK.

The demo code included uses PIO to blink the on-board LED.

It demonstrates:
 - adding source files
 - adding header files
 - adding libraries
 - PIO syntax
 - adding a PIO program
 - handling a PIO IRQ
