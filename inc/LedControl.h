/* pico_template/inc/LedControl.h
 * written in 2021 by Miles Courtie for CircuitWorx
 */

#ifndef LedControl_h
#define LedControl_h

#include "pico/stdlib.h"

void LedControl_init();

void LedControl_blink(uint32_t count);

#endif // LedControl_h