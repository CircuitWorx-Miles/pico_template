cmake_minimum_required(VERSION 3.13)

# import Pico SDK
include(pico_sdk_import.cmake)

# project name & languages
project(pico_template C CXX ASM)
set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)

# remove all optimisations from debug builds
set(ENV{PICO_DEOPTIMIZED_DEBUG} 1)

# initialise Pico SDK
pico_sdk_init()

# add executable and its source files
add_executable(pico_template
	src/main.c

	inc/LedControl.h
	src/LedControl.c
)

# add PIO program
pico_generate_pio_header(pico_template
	${CMAKE_CURRENT_LIST_DIR}/src/LedBlink.pio)

# add include directory
target_include_directories(pico_template PRIVATE
	inc
)

# add libraries as dependency
target_link_libraries(pico_template
	pico_stdlib
	hardware_pio
	hardware_irq
)

# build extra outputs (.uf2, disassembly, etc.)
pico_add_extra_outputs(pico_template)

# enable/disable routing stdio to USB
pico_enable_stdio_usb(pico_template 1)

# enable/disable routing stdio to UART
pico_enable_stdio_uart(pico_template 1)
